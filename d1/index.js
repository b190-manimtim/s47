// Using DOM
// before
// document.getElementById('txt-first-name')
// document.getElementsByClassName('txt-last-name')
// document.getElementsByTagName('input')

// query selector
document.querySelector('#txt-first-name')

// Event Listeners
const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullname = document.querySelector('#span-full-name')

txtFirstName.addEventListener('keyup', (event)=>{
    spanFullname.innerHTML = txtFirstName.value
})

txtFirstName.addEventListener('keyup', (event)=>{
    console.log(event.target)
    console.log(event.target.value)
})

// txtLastName.addEventListener('keyup', (event)=>{
//     spanFullname.innerHTML = txtLastName.value
// })