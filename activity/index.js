
document.querySelector('#txt-first-name');


const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

let fullName = () => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

document.addEventListener("keyup", (event) => 
{
    fullName();
});